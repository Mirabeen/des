##################
### Librairies ###
##################


from random import randint
import json
from settings import *


#############
### Class ###
#############


class SetDes:
    """
    Cette Class defini le jeu complet
    """
    
    def __init__(self, p=9997, h='0.0.0.0', server=None):
        """
        Initialisation du jeu lors du lancement du server
        :param p: int, port de connexion
        :param h: str, ip de connexion (127.0.0.1 pour localhost seulement, 0.0.0.0 pour acces publique)
        """
        
        # Initialisation
        self.port = p
        self.host = h
        self.server = server
        self.des_lock = False
        self.sets_des = dict()
        self.nb_s_d = 0
        self.action = ''
        self.retour_message = ''
        
        # Message de lancement
        self.aff_nom_jeu()
    
    def aff_nom_jeu(self):
        """
        Message de bienvenue
        """
        
        print(''.center(60, '#'))
        print('#####' + 'Lancement du Serveur Set de Dés'.center(50) + '#####')
        print('#####' + ('   PORT : %d   ' % self.port).center(50) + '#####')
        print('#####' + ('   HOST : %s   ' % self.host).center(50) + '#####')
        print(''.center(60, '#') + '\n\n')
    
    def play(self, sets_des_in, nb_s_d):
        """
        Cette fontion simule un lancer de des ainsi que les actions qui en dependent
        :param sets_des: dico, Sets de des a lancer
        :param nb_s_d: int, Nombre total de son de des si 0 alors on ne joue pas mais on met juste a jour les sets
        chez tout le monde
        """
        
        # Lancement des des
        self.sets_des = dict()
        for s in sets_des_in.keys():
            self.sets_des[s] = {'values': list(), 'nb': sets_des_in[s]['nb'], 'min': sets_des_in[s]['min'],
                                'max': sets_des_in[s]['max']}
            if nb_s_d != 0:
                for _ in range(sets_des_in[s]['nb']):
                    self.sets_des[s]['values'].append(randint(sets_des_in[s]['min'], sets_des_in[s]['max']))
        if nb_s_d != 0:
            self.des_lock = True
            self.nb_s_d = randint(1, nb_s_d)
    
    def etat_jeu(self):
        """
        Affichage de l'etat du jeu pour envoie aux clients
        :return: str, etat du jeu a renvoyer aux clients
        """
        
        return json.dumps(self.__dict__, indent=4, default=lambda o: '<###>')
    
    def traite_message(self, client, server, message):
        """
        Traite le message recu du client
        :param client: dico, client qui a envoye le message
        :param server: websocket, serveur websocket
        :param message: str, message recu
        :return: message envoye a tout les clients
        """
        
        # Type de message recu
        self.retour_message = ''
        type_mess = eval(message)['type_mess']
        
        # Lors d'un lancecement de des
        if type_mess == 'joue':
            sets_des = eval(message)['sets_des']
            nb_s_d = eval(message)['nb_s_d']
            self.play(sets_des, nb_s_d)
            self.action = 'joue'
        # Maj sets de des sans joueur
        elif type_mess == 'majsets':
            sets_des = eval(message)['sets_des']
            self.play(sets_des, 0)
            self.action = 'majsets'
        # Alive du client
        elif type_mess == 'stayalive':
            self.action = 'stayalive'
            server.send_message(client, self.etat_jeu())
            return
        # Autorisation des
        elif type_mess == 'allowdice':
            self.action = 'allowdice'
            self.des_lock = False
        # Cas on a pas compris ce qu'on voulait (normalement on passe pas par la)
        else:
            self.action = 'echec'
        server.send_message_to_all(self.etat_jeu())

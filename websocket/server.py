##################
### Librairies ###
##################


from websocket_server import WebsocketServer
import main
from settings import *


#################
### Callbacks ###
#################


def new_client(client, server):
    """
    Called for every client connecting (after handshake)
    """
    
    if client is None:
        return
    if 'id' in client.keys():
        print("Connexion d'un nouveau client, id : %d..." % client['id'])
    print('Nombre de clients : %d' % len(server.clients))


def client_left(client, server):
    """
    Called for every client disconnecting
    """
    
    if client is None:
        return
    if 'id' in client.keys():
        print('%d deconnecte !' % client['id'])
    print('Nombre de clients : %d\n\n' % (len(server.clients) - 1))


def message_received(client, server, message):
    """
    Called when a client sends a message
    """
    
    jeu.traite_message(client, server, message)


#################
### Lancement ###
#################


if __name__ == "__main__":
    # Creation du server websocket
    server = WebsocketServer(port=PORT, host=HOST)
    # Assignation des callbacks au server
    server.set_fn_new_client(new_client)
    server.set_fn_client_left(client_left)
    server.set_fn_message_received(message_received)
    # Creation de la Class Freeman du jeu
    jeu = main.SetDes(PORT, HOST, server)
    # Run le server
    server.run_forever()

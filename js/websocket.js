/*****************
*** Websockets ***
*****************/
var ws;
var data;
var aliveID = 0;

function init() {
    // Connect to Web Socket
    myLog('Connexion...');
    ws = new WebSocket("ws://" + ws_url + ":" + ws_port + "/");

    // Set event handlers
    ws.onopen = function() { stayAlive(); };

    ws.onmessage = function(e) {
        const json = e.data;
        data = JSON.parse(json);
        if (data['retour_message'] != '') { myLog(data['retour_message']); }
        if (data['action'] == 'joue') {
            play(1);
        } else if (data['action'] == 'majsets') {
            play(0);
        }
    };

    ws.onclose = function() { aliveStop(); };

    ws.onerror = function(e) { myLog(e.data); };
    myLog('Ok !');
}

function onSubmit(e) {
    // Lancement des des
    if (e == 0 || e ==1) {
        // Verification qu'on est connecte
        if (ws['readyState'] != 1) {
            alert("Le serveur semble ne pas répondre\nEssayer de recharger la page");
            myLog("Le serveur semble ne pas répondre\nEssayer de recharger la page");
            return;
        }
        // Verification qu'on a le droit de jouer
        if (data['des_lock']) {
            myLog('Les des sont deja en cours de lancement');
            des_lock++;
            if (des_lock > 5) {
                data['des_lock'] = false;
                des_lock = 0;
            }
            return;
        }
        // Objet du DOM
        const des = document.getElementById('des');
        const nb_set = des.children.length / 2 + 1;
        // Recuperation de tout les des a jouer
        var sets_des = {};
        for (i = 1; i < nb_set; i++) {
            var desi = document.getElementById(i);
            var nb_desi = desi.getElementsByClassName("nb_des")[0].value;
            var nb_facei = desi.getElementsByClassName("nb_face")[0].value;
            sets_des[i] = {'nb': Number(nb_desi), 'min': 1, 'max': Number(nb_facei)};
        }
        if (e == 0) {
            // Jouage
            des_lock = 0;
            ws.send(JSON.stringify({'type_mess': 'joue', 'nb_s_d': Object.keys(audio_files).length, 'sets_des': sets_des}));
        } else if (e == 1) {
            // Maj Sets
            ws.send(JSON.stringify({'type_mess': 'majsets', 'sets_des': sets_des}));
        }
    // Message de survie
    } else if (e == 2) {
        ws.send(JSON.stringify({'type_mess': 'stayalive'}));
    // Autorisation des des
    } else if (e == 3) {
        ws.send(JSON.stringify({'type_mess': 'allowdice'}));
    }
}

function onCloseClick() { ws.close(); }

function stayAlive() {
    onSubmit(2);
    aliveID = setTimeout(function() { stayAlive(); }, alive_timer);
}

function aliveStop() { clearTimeout(aliveID); }

/*****************
*** Constantes ***
*****************/
const img_innerHTML = '<img src="images/$i$.gif" alt="Image du dés $j$" title="Dés $j$" class="des"/>';
var data = {};
var des_lock = 0;
// sons
var audio1 = new Audio('sons/Des1.mp3'); // Le son a jouer au lancement du Des1
var audio2 = new Audio('sons/Des2.mp3'); // Le son a jouer au lancement du Des2
var audio3 = new Audio('sons/Des3.mp3'); // Le son a jouer au lancement du Des3
var audio4 = new Audio('sons/Des4.mp3'); // ...
var audio5 = new Audio('sons/Des5.mp3');
var audio6 = new Audio('sons/Des6.mp3');
var audio7 = new Audio('sons/Des7.mp3');
var audio8 = new Audio('sons/Des8.mp3');
var audio9 = new Audio('sons/Des9.mp3');
var audio10 = new Audio('sons/Des10.mp3');
var audio_files = {
    1: {'audio': audio1, 'time': 2000},
    2: {'audio': audio2, 'time': 1300},
    3: {'audio': audio3, 'time': 1600},
    4: {'audio': audio4, 'time': 2100},
    5: {'audio': audio5, 'time': 1800},
    6: {'audio': audio6, 'time': 2100},
    7: {'audio': audio7, 'time': 3900},
    8: {'audio': audio8, 'time': 1400},
    9: {'audio': audio9, 'time': 1300},
    10: {'audio': audio10, 'time': 2600}
}; // Dico reprennant tout les sons a jouer ainsi que leur duree lors du lancement de des


/****************
*** Fonctions ***
****************/
// Date courrante
function getCurrentDate() {
    // Variable de la date
    var d = new Date;
    var sec = d.getSeconds();
    var min = d.getMinutes();
    var heu = d.getHours();
    var day = d.getDate();
    var mon = d.getMonth() + 1;
    var yea = d.getFullYear();
    if (sec < 10) {
        sec = "0" + sec;
    }
    if (min < 10) {
        min = "0" + min;
    }
    if (heu < 10) {
        heu = "0" + heu;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (mon < 10) {
        mon = "0" + mon;
    }
    var my_date = '[' + yea + '-' + mon + '-' + day + '_' + heu + ':' + min + ':' + sec + '] ';
    return my_date;
}

// Fonction de Log
function myLog(mess) {
    // Recuperation de la date
    var my_date = getCurrentDate();
    // Ecriture du message dans la console
    console.log(my_date + mess);
}

// Jouage du son des des
async function playAudio(nb_s_d) {
    try {
        await audio_files[nb_s_d]['audio'].play();
    } catch (err) {
        myLog('Failed to play...' + err);
    }
}

// Nombre aleatoire
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max) + 1;
    return Math.floor(Math.random() * (max - min)) + min;
}

// Met a jour le nombre de des et le nombre de face du set considere
function update(v) {
    // Objet du DOM
    const setv = document.getElementById(v);
    const desv = document.getElementById('des_' + v);
    const nb_desv = setv.getElementsByClassName("nb_des")[0].value;
    const nb_facev = setv.getElementsByClassName("nb_face")[0].value;
    // Ecriture de l'HTML pour modifier le nombre de des avec la face max
    var curr_innerHTML = '';
    for (i = 0; i < nb_desv; i++) {
        curr_innerHTML = curr_innerHTML + img_innerHTML.replace('$i$', nb_facev).replace('$j$', i + 1).replace('$j$', i + 1);
    }
    desv.innerHTML = curr_innerHTML;
    // Maj des value des boutons
    setv.getElementsByClassName("nb_des")[0].setAttribute('value', nb_desv);
    setv.getElementsByClassName("nb_face")[0].setAttribute('value', nb_facev);
    // Maj des sets au server
    onSubmit(1);
}

// Ajoute un set de des
function ajoutSetDes() {
    // Objet du DOM
    const des = document.getElementById('des');
    const nb_set_m = des.children.length / 2;
    const nb_set = nb_set_m + 1;
    const set_m = document.getElementById(nb_set_m);
    const retrait_serie = document.getElementById('retrait_serie');
    // Ajout
    var curr_innerHTML = des.innerHTML;
    curr_innerHTML = curr_innerHTML + '<table id="' + (nb_set) + '">' + set_m.innerHTML.replace("n°" + nb_set_m, "n°" + nb_set).replace("update('" + nb_set_m + "');", "update('" + nb_set + "');").replace("update('" + nb_set_m + "');", "update('" + nb_set + "');") + '</table>';
    curr_innerHTML = curr_innerHTML + '<div id="des_' + (nb_set) + '" class="conteneur_des">' + img_innerHTML + '</div>';
    des.innerHTML = curr_innerHTML;
    // Objet du DOM
    const setv = document.getElementById(nb_set);
    const desv = document.getElementById('des_' + nb_set);
    // Maj des value des boutons
    update(nb_set);
    // Ajout du bouton de retrait
    retrait_serie.style.display = 'block';
}

// Retire un set de des
function enleveSetDes() {
    // Objet du DOM
    const des = document.getElementById('des');
    const nb_set = des.children.length / 2;
    const retrait_serie = document.getElementById('retrait_serie');
    // Verif du nombre de set pour ne pas tout virer
    if (nb_set == 2) { retrait_serie.style.display = 'none'; }
    // Retrait
    var curr_innerHTML = '';
    for (i = 1; i < nb_set; i++) {
        const seti = document.getElementById(i);
        const desi = document.getElementById('des_' + i);
        curr_innerHTML = curr_innerHTML + '<table id="' + (i) + '">' + seti.innerHTML + '</table>';
        curr_innerHTML = curr_innerHTML + '<div id="des_' + (i) + '" class="conteneur_des">' + desi.innerHTML + '</div>';
    }
    des.innerHTML = curr_innerHTML;
}

// Modifie l'affichage de la valeur du des j appartenant a la totalite des des
function setDesVal(j) {
    var des_all = document.getElementsByClassName('des');
    var desj = des_all[j];
    var p_des = document.getElementById(desj.parentNode.id.split('_')[1]).getElementsByClassName("nb_face")[0];
    var min = 1;
    var max = p_des.value;
    desj.setAttribute('src', 'images/' + getRandomInt(min, max) + '.gif');
}

// Modifie l'affichage de la valeur du des j du set i
function setDesValVrai(i, j, val) {
    var des = document.getElementById('des_' + i).getElementsByClassName('des')[j];
    des.setAttribute('src', 'images/' + val + '.gif');
}

// Touille les des
function touilleDes(des_all, nb_t, t) {
    for (let i = 0; i < nb_t; i++) {
        for (let j = 0; j < des_all.length; j++) {
            setTimeout(function() { setDesVal(j); }, i * t / nb_t);
        }
    }
}

// Affiche les vrais valeurs des des
function afficheDes(t) {
    for (let k = 0; k < Object.keys(data['sets_des']).length; k++) {
        for (let l = 0; l < data['sets_des'][Object.keys(data['sets_des'])[k]]['values'].length; l++) {
            setTimeout(function() { setDesValVrai(Object.keys(data['sets_des'])[k], l, data['sets_des'][Object.keys(data['sets_des'])[k]]['values'][l]); }, t);
        }
    }
}

// Reception de type jeu
function play(e) {
    // Objet du DOM
    const des_all = document.getElementsByClassName('des');
    t = 0;
    if (e == 1) {
        // On joue un son au lancement du des
        nb_s_d = data['nb_s_d'];
        t = audio_files[nb_s_d]['time'];
        nb_t = t / 100;
        playAudio(nb_s_d);
        // On touille les des
        touilleDes(des_all, nb_t, t);
        // Autorisation des des pour le tour suivant
        setTimeout(function() { onSubmit(3); }, t + allow_dice_timer);
    }
    // Affichage du vrai lancer
    afficheDes(t);
}

// Initialisation du plateau pour les connexion
function initPlateau() {
    // Verif que le jeu avait commencer
    if (Object.keys(data['sets_des']).length == 0) {
        return;
    }
    // Objet du DOM
    const set1 = document.getElementById('1');
    const nb_des1 = set1.getElementsByClassName("nb_des")[0];
    const nb_face1 = set1.getElementsByClassName("nb_face")[0];
    // Cas du premier Set
    nb_des1.value = data['sets_des']['1']['nb'];
    nb_face1.value = data['sets_des']['1']['max'];
    update('1');
    // Puis les autres
    for (let i = 2; i < Object.keys(data['sets_des']).length + 1; i++) {
        seti = document.getElementById(i);
        if (seti == null) {
            ajoutSetDes();
            seti = document.getElementById(i);
        }
        const nb_desi = seti.getElementsByClassName("nb_des")[0];
        const nb_facei = seti.getElementsByClassName("nb_face")[0];
        nb_desi.value = data['sets_des'][i]['nb'];
        nb_facei.value = data['sets_des'][i]['max'];
        update(i);
    }
    afficheDes(0);
}

// Mise a jour au lancement
setTimeout( function() { init(); }, 200);
setTimeout( function() { initPlateau(); }, 1000);
